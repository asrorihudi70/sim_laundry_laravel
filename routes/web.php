<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controllers_admin;
use App\Http\Controllers\Controllers_main;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', function () {
//     return view('home');
// });

Route::get('/dashboard', function () {
    return view('dashboard');
});

// Route::resource('adm', Admin::class);

// Route::get('/admin', function () {
//     return view('admin');
// });

//route CRUD
Route::get('/', [Controllers_main::class, 'home']);
Route::get('/admin', [Controllers_admin::class, 'index']);
Route::get('/admin/add', [Controllers_admin::class, 'add']);

// Route::post('/add_admin', [Controllers_admin::class, 'add_admin']);
 
Route::controller(Controllers_admin::class)->group(function () {
    // Route::get('/orders/{id}', 'show');
    Route::post('/add_admin', 'add_admin');
});