<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Admin</title>
</head>
<body>
    <br>
    <br>
    <center>
    <h2>Trial</h2>
	<h3>Data Admin</h3>
 
	<a href="<?php URL::to('/'); ?>admin/add"> + Tambah Admin Baru</a>
	
	<br/>
	<br/>

        <table border='1' cellspacing='0'>
            <tr>
                <th>ID Admin</th>
                <th>Nama Admin</th>
                <th>User Name</th>
                <th>Password</th>
                <th>&nbsp;</th>
            </tr>
            @foreach($admin as $rec)
            <tr>
                <td>{{ $rec->id_admin }}</td>
                <td>{{ $rec->nama_admin }}</td>
                <td>{{ $rec->username }}</td>
                <td>{{ $rec->password }}</td>
                <td>
                    <a href="">Edit</a>
                    |
                    <a href="">Hapus</a>
                </td>
            </tr>
            @endforeach
        </table>
    </center>
</body>
</html>