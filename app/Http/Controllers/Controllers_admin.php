<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Controllers_admin extends Controller
{
    //
    public function index(){
        $admin = DB::table('admin')->get();
        return view('admin',['admin'=>$admin]);
    }

    public function add()
    {
        // memanggil view tambah
        return view('add');
    }

    public function add_admin(){
        $response = array();
        // $response['data'] = $_POST['name'];
        $nama       = $_POST['name'];
        $username   = $_POST['username'];
        $password   = $_POST['password'];

        $result = DB::table('admin')->insert(
            [
                'id_admin' => '1012', 
                'nama_admin' => $nama,
                'username' => $username,
                'password' => $password
            ]
        );
        
        if($result){
            $response['status'] = true;
            $response['message'] = 'Data admin berhasil disimpan.';
        }else{
            $response['status'] = false;
            $response['message'] = 'Data admin gagal disimpan.';
        }
        return json_encode($response);
    }
}
